/* problem #6
* projecteuler.net
* The sum of the squares of the first 10 natural numbers is:
* 1^2 + 2^2 + ... + 10^2 = 385
* The square of the sums of the first 10 natural numbers is:
* (1 + 2 + ... + 10)^2 = 55^2 = 3025
* The difference is 2640
*
* Do this for the first 100 natural numbers
*/

#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char * argv[])
{
	int sum_of_squares 	= 0;
	int square_of_sums 	= 0;	
	int max,i,j;

	if (!argv[1]) {
		puts("You must provide a number\n");
		exit(1);
	}

	max = atoi(argv[1]);

	for(i = 1; i <= max; i++) {
		sum_of_squares += (i*i);
	}

	for(j = 1; j <= max; j++) {
		square_of_sums += j;
	}

	printf("Answer: %i\n", ((square_of_sums*square_of_sums) - sum_of_squares));
	
	return(0);
}
