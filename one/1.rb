# If we list all the natural numbers below 10 that are multiples of 3 or 5,
# we get 3, 5, 6 and 9. The sum of these multiples is 23.

# Find the sum of all the multiples of 3 or 5 below 1000.

class NaturalNumbers

  def initialize
  end

  def list_natural_numbers_below(num)
    [*1...num]
  end

  def list_multiples_of_three_or_five(nums)
    nums.select{|n| n if multiple_of_three_or_five(n)}
  end

  def multiple_of_three_or_five(num)
    num % 3 == 0 || num % 5 == 0
  end

  def sum_list(nums)
    total = 0
    nums.each {|n| total += n}
    total
  end

  def give_me_an_answer
    nums = self.list_natural_numbers_below(1000)
    shorter_list = self.list_multiples_of_three_or_five(nums)
    self.sum_list(shorter_list)
  end

end


