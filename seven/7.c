/* problem #7
 * projecteuler.net
 * By listing the first 6 prime numbers: 2, 3, 5, 7, 11, and 13 we see that the 6th prime is 13.
 * What is the 10001st prime?
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// miller-rabin primality test

struct miller_rabin {
      int s;
      int d;
};

struct miller_rabin variables;


int
is_odd(number)
{
      return(number % 2);
}

void
factor_powers_of_two(number)
{
      int n1 = number - 1;
      int s = 0;

      while (!(n1 % 2)) {
	    n1 /= 2;
	    s++;
      }
      variables.s = s;
      variables.d = n1;
}

int
choose_random(number)
{
      /* return 2 .. n - 2 */
      int r;

      r = rand() % (number - 1);

      if (r < 2) r = 2;

      return(r);
}

int
power_of(num, power)
{
      int i;
      int res;
      
      res = 1;

      for(i = 1; i <= power; i++) {
	    res *= num;
      }
      return(res);
}

int
is_prime(int number)
{
      int i;
      int is_prime = 1;

      for(i = 2; i < number; i++) {
	    if(fmod(number, i) == 0) {
		  is_prime = 0;
		  break;
	    }
      }
      return(is_prime);
}

int
is_prime_by_miller_rabin(n)
{
      int accuracy;
      int a, x;
      int i, r;

      factor_powers_of_two(n);
      
      printf("DEBUG: s == %i; d == %i\n", variables.s, variables.d);

      for (accuracy = 0; accuracy < 10; accuracy++) {
	    a = choose_random(n);

	    x = power_of(a, variables.d) % n;

	    if ((x == 1) || (x == (n - 1))) continue;

	    for (r = 1; r < variables.s - 1; r++) {
		  x = power_of(x, 2) % n;
		  if (x == 1) return(0);              /* composite      */
		  if (x == (n - 1)) continue;
	    }

	    return(0);                                /* composite      */

      }
      return(1);                                      /* probably prime */
}

int
main(int argc, char * argv[])
{

      int number, prime_count;

      int prime_number_we_want = 10001;

      int quick, quick_number;

      quick = ((argv[1]) ? (1) : (0));

      if (quick) {
	    /* single number calc */
	    quick_number = atoi(argv[1]);
	    /* printf("%i is%s prime\n", quick_number, (is_prime_by_miller_rabin(quick_number)) ? ("") : (" NOT")); */
	    printf("%i is%s prime\n", quick_number, (is_prime(quick_number)) ? ("") : (" NOT"));
      } else {
	    /* do the euler problem */
	    number   = 5;

	    while (prime_count != prime_number_we_want) {
		  if (is_odd(number)) {
			/* if (is_prime_by_miller_rabin(number)) { */
			if (is_prime(number)) {
			      printf("DEBUG found prime: %i, prime count is %i\n", number, prime_count);
			      prime_count++;
			}
			number += 1;
		  } else {
			number += 1;
			continue;
		  }
	    }

	    printf("the %ith prime is %i\n", prime_number_we_want, number);
      }

      return(0);
}
